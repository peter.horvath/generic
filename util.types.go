package generic

import (
  "reflect"
  "hash/crc32"
  "strconv"
)

func GetTypeName(t reflect.Type) string {
  //fmt.Printf("t type: %s, kind:%s\n", reflect.TypeOf(t).Name(), reflect.TypeOf(t).Kind().String())
  k := t.Kind()
  if ((k <= reflect.Complex128) && (k != reflect.Uintptr)) || (k == reflect.String) {
    return k.String()
  } else if (k == reflect.Interface) || (k == reflect.Ptr) {
    elem := t.Elem()
    return elem.PkgPath() + "." + elem.Name()
  } else if k == reflect.Struct {
    return t.PkgPath() + "." + t.Name()
  } else {
    panic("TODO: k = " + strconv.Itoa(int(k)) + " (" + k.String() + ")")
  }
}

func GetTypeHash(t reflect.Type) int32 {
  return int32(crc32.Checksum([]byte(GetTypeName(t)), crc32.IEEETable) & 0x7fffffff)
}
