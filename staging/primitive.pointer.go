package generic

import (
  "fmt"
  "unsafe"
  "reflect"
)

var Typepointer reflect.Type
var TypePointer reflect.Type

func compare_pointer_pointer(a interface{}, b interface{}) int {
  aa := a.(ptr)
  bb := b.(ptr)
  if (aa < bb) {
    return -1
  } else if (aa > bb) {
    return 1
  } else {
    return 0
  }
}

type Pointer struct {
  Object
  val interface{}
}

func NewPointer(v interface{}) *Pointer {
  p := new (Pointer)
  p.val = v
  return p;
}

func (p *Pointer) GetValue() interface{} {
  return p.val
}

func (p *Pointer) SetValue(v interface{}) {
  p.val = v;
}

func (p *Pointer) Clone() interface{} {
  return NewPointer(p.val)
}

func (p *Pointer) Compare(i interface{}) int {
  return ComparePointer(p, i.(*Pointer))
}

func (p *Pointer) ToString() *String {
  return NewString(fmt.Sprintf("%p", p.val))
}

func ComparePointer(a interface{}, b interface{}) int {
  aa := uintptr(unsafe.Pointer(&(a.(Pointer).val)))
  bb := uintptr(unsafe.Pointer(&(b.(Pointer).val)))
  if aa < bb {
    return -1
  } else if aa > bb {
    return 1
  } else {
    return 0
  }
}

func initPointer() {
  Typepointer = reflect.TypeOf((*ptr)(nil)).Elem()
  TypePointer = reflect.TypeOf((*Pointer)(nil)).Elem()
  RegisterComparator(Typepointer, Typepointer, compare_pointer_pointer)
  RegisterComparator(TypePointer, TypePointer, ComparePointer)
}
