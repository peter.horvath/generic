package generic

import (
  "testing"
  "fmt"
)

func TestMultiTuple(test *testing.T) {
  fmt.Println("multituple_test start")
  mt := NewMultiTuple()
  for i := 0; i < 10; i++ {
    fmt.Printf("adding %d mt=%p\n", i, mt)
    mt.Set(i, i*i)
    fmt.Printf("mt.size: %d\n", mt.GetSize())
    fmt.Println(mt.ToString())
  }
  fmt.Println("multituple_test end")
}
