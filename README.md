Forked initally from https://github.com/ancientlore/go-avltree . No code left here from the original repo.

Re-issued with GPLv3.

Re-issued with LGPLv3.

Note: You *can* compare here primitive.Pointer, but note: it is *unsafe*, the
GC *can* move the underlying object and so it will be buggy!
