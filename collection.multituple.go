package generic

import (
  "reflect"
  "fmt"
)

var MultiTupleType *Type
var MultiTupleIteratorType *Type

type MultiTuple struct {
  Object
  mainList TreeList
  keyMap Map
}

type mtNode struct {
  mainListIter *TreeListIterator
  keyMapIter *MapIterator
  listIter *TreeListIterator
  key interface{}
  value interface{}
}

type MultiTupleIterator struct {
  Object
  mt *MultiTuple
  node *mtNode
}

func NewMultiTuple() *MultiTuple {
  return new(MultiTuple).Init()
}

func (m *MultiTuple) Init() *MultiTuple {
  m.type_ = MultiTupleType
  m.mainList.Init()
  m.keyMap.Init()
  return m
}

func (mt *MultiTuple) Add(key interface{}, value interface{}) *MultiTupleIterator {
  return mt.AddLast(key, value)
}

func (mt *MultiTuple) AddFirst(key interface{}, value interface{}) *MultiTupleIterator {
  mtn := mt.add(key, value)
  mtn.mainListIter = mt.mainList.AddFirst(mtn)
  return newMtIterator(mt, mtn)
}

func (mt *MultiTuple) AddLast(key interface{}, value interface{}) *MultiTupleIterator {
  fmt.Printf("mt.AddLast, mt: %p\n", mt)
  mtn := mt.add(key, value)
  mtn.mainListIter = mt.mainList.AddLast(mtn)
  return newMtIterator(mt, mtn)
}

func (mt *MultiTuple) At(idx int64) *MultiTupleIterator {
  if (idx < 0) || (idx >= mt.mainList.GetSize()) {
    return nil
  } else {
    return newMtIterator(mt, mt.mainList.At(idx).GetValue().(*mtNode))
  }
}

func (mt *MultiTuple) Find(key interface{}) *MultiTupleIterator {
  kmi := mt.keyMap.Find(key)
  if kmi.IsNull() {
    return NewMultiTupleIterator()
  } else {
    return newMtIterator(mt, kmi.GetValue().(*Pair).value.(*TreeList).GetFirst().GetValue().(*mtNode))
  }
}

func (mt *MultiTuple) Get(key interface{}) interface{} {
  return mt.Find(key).GetValue()
}

func (mt *MultiTuple) GetFirst() *MultiTupleIterator {
  return newMtIterator(mt, mt.mainList.GetFirst().GetValue().(*mtNode))
}

func (mt *MultiTuple) GetKeySet() *Set {
  return mt.keyMap.GetKeySet()
}

func (mt *MultiTuple) GetLast() *MultiTupleIterator {
  return newMtIterator(mt, mt.mainList.GetLast().GetValue().(*mtNode))
}

func (mt *MultiTuple) GetPairList() *TreeList {
  r := NewTreeList()
  for i := mt.GetFirst(); !i.IsNull(); i.Step() {
    //fmt.Printf("iter: %d %d\n", i.GetKey().(int), i.GetValue().(int))
    r.Add(NewPair(i.GetKey(), i.GetValue()))
  }
  return r
}

func (mt *MultiTuple) GetSize() int64 {
  return mt.mainList.GetSize()
}

func (mt *MultiTuple) Has(key interface{}) bool {
  return mt.keyMap.Has(key)
}

func (mt *MultiTuple) Insert(idx int64, key interface{}, value interface{}) *MultiTupleIterator {
  mtn := mt.add(key, value)
  mtn.mainListIter = mt.mainList.Insert(idx, mtn)
  return newMtIterator(mt, mtn)
}

func (mt *MultiTuple) IsEmpty() bool {
  return mt.mainList.IsEmpty()
}

func (mt *MultiTuple) Pop() *Pair {
  mti := mt.GetFirst().Remove()
  return NewPair(mti.node.key, mti.node.value)
}

func (mt *MultiTuple) PopLast() *Pair {
  mti := mt.GetLast().Remove()
  return NewPair(mti.node.key, mti.node.value)
}

func (mt *MultiTuple) Set(key interface{}, value interface{}) *MultiTuple {
  mti := mt.Find(key)
  if mti.IsNull() {
    mt.Add(key, value)
  } else {
    mti.SetValue(value)
  }
  return mt
}

func (mt *MultiTuple) Unset(key interface{}) *MultiTuple {
  kmi := mt.keyMap.Find(key)
  if kmi != nil {
    mtNodeList := kmi.GetValue().(*TreeList)
    for {
      if mtNodeList.IsEmpty() {
        break;
      }
      mtNodeList.Pop().(*mtNode).mainListIter.Remove()
    }
  }
  return mt
}

func (mt *MultiTuple) ToString() string {
  s := ConcatStrings("[", mt.GetPairList().Join(","), "]")
  return s
}

func NewMultiTupleIterator() *MultiTupleIterator {
  return new(MultiTupleIterator).Init()
}

func (mti *MultiTupleIterator) Init() *MultiTupleIterator {
  mti.type_ = MultiTupleIteratorType
  return mti
}

func (mti *MultiTupleIterator) AddAfter(key interface{}, value interface{}) *MultiTupleIterator {
  mtn := mti.mt.add(key, value)
  mtn.mainListIter = mti.node.mainListIter.AddAfter(mtn)
  return newMtIterator(mti.mt, mtn)
}

func (mti *MultiTupleIterator) AddBefore(key interface{}, value interface{}) *MultiTupleIterator {
  mtn := mti.mt.add(key, value)
  mtn.mainListIter = mti.node.mainListIter.AddBefore(mtn)
  return newMtIterator(mti.mt, mtn)
}

func (mti *MultiTupleIterator) GetIdx() int64 {
  return mti.node.mainListIter.GetIdx()
}

func (mti *MultiTupleIterator) GetKey() interface{} {
  return mti.node.key
}

func (mti *MultiTupleIterator) GetMultiTuple() *MultiTuple {
  return mti.mt
}

func (mti *MultiTupleIterator) GetNext() *MultiTupleIterator {
  mtn := mti.node
  if mtn.mainListIter.IsLast() {
    return NewMultiTupleIterator().SetMultiTuple(mti.mt)
  } else {
    return newMtIterator(mti.mt, mtn.mainListIter.GetNext().GetValue().(*mtNode))
  }
}

func (mti *MultiTupleIterator) GetPrev() *MultiTupleIterator {
  mtn := mti.node
  if mtn.mainListIter.IsFirst() {
    return NewMultiTupleIterator().SetMultiTuple(mti.mt)
  } else {
    return newMtIterator(mti.mt, mtn.mainListIter.GetPrev().GetValue().(*mtNode))
  }
}

func (mti *MultiTupleIterator) GetValue() interface{} {
  return mti.node.value
}

func (mti *MultiTupleIterator) IsFirst() bool {
  return mti.node.mainListIter.IsFirst()
}

func (mti *MultiTupleIterator) IsLast() bool {
  return mti.node.mainListIter.IsLast()
}

func (mti *MultiTupleIterator) IsNull() bool {
  return (mti.mt == nil) || (mti.node == nil)
}

func (mti *MultiTupleIterator) Remove() *MultiTupleIterator {
  mtn := mti.node
  mtn.mainListIter.Remove()
  mtn.listIter.Remove()
  if mti.mt.keyMap.Get(mtn.key).(*TreeList).IsEmpty() {
    mti.mt.keyMap.Unset(mtn.key)
  }
  mti.mt = nil
  return mti
}

func (mti *MultiTupleIterator) SetMultiTuple(mt *MultiTuple) *MultiTupleIterator {
  mti.mt = mt
  if (mt == nil) || (mt.IsEmpty()) {
    mti.node = nil
  } else {
    mti.node = mt.mainList.GetFirst().GetValue().(*mtNode)
  }
  return mti
}

func (mti *MultiTupleIterator) SetIdx(idx int64) *MultiTupleIterator {
  mti.node.mainListIter.SetIdx(idx)
  return mti
}

func (mti *MultiTupleIterator) SetKey(key interface{}) *MultiTupleIterator {
  value := mti.node.value
  mti.Remove()
  mti2 := mti.mt.Add(key, value)
  mti.mt = mti2.mt
  mti.node = mti2.node
  return mti
}

func (mti *MultiTupleIterator) SetValue(value interface{}) *MultiTupleIterator {
  mti.node.value = value
  return mti
}

func (mti *MultiTupleIterator) Step() *MultiTupleIterator {
  if mti.node.mainListIter.IsLast() {
    mti.node = nil
  } else {
    mti.node = mti.node.mainListIter.GetNext().GetValue().(*mtNode)
  }
  return mti
}

func (mti *MultiTupleIterator) StepBack() *MultiTupleIterator {
  if mti.node.mainListIter.IsFirst() {
    mti.node = nil
  } else {
    mti.node = mti.node.mainListIter.GetPrev().GetValue().(*mtNode)
  }
  return mti
}

/* Helper to the Add/Insert methods: inserts a new key/value pair into the MultiTuple,
 * BUT: it does not put it into the mainList, and also does not set the mainListIter of
 * the mtNode! It is the post-fix task of the callers. */
func (mt *MultiTuple) add(key interface{}, value interface{}) *mtNode {
  //fmt.Printf("mt.add, mt: %p, key: %p, value: %p\n", mt, key, value)
  mtn := newMtNode()
  mtn.keyMapIter = mt.keyMap.Find(key)
  if mtn.keyMapIter.IsNull() {
    mtn.keyMapIter = mt.keyMap.Add(NewPair(key, NewTreeList()))
  }
  mtn.listIter = mtn.keyMapIter.GetValue().(*TreeList).AddLast(mtn)
  mtn.key = key
  mtn.value = value
  return mtn
}

func newMtNode() *mtNode {
  mtn := new(mtNode)
  fmt.Printf("exists\n")
  mtn.mainListIter = NewTreeListIterator()
  mtn.keyMapIter = NewMapIterator()
  mtn.listIter = NewTreeListIterator()
  mtn.key = nil
  mtn.value = nil
  return mtn
}

/* helper to Pop() and PopLast(): removes all elements of a list of mtNodes from the mainList of the
 * MultiTuple, and converts them to their values
 */
func mtNodeListToValueList(tl *TreeList) {
  for i := tl.GetFirst(); !i.IsNull(); i.Step() {
    mtn := i.GetValue().(*mtNode)
    mtn.mainListIter.Remove()
    i.SetValue(mtn.value)
  }
}

/* Helper: creates a new MultiTupleIterator pointing to the given MultiTuple and mtNode */
func newMtIterator(mt *MultiTuple, mtn *mtNode) *MultiTupleIterator {
  mti := NewMultiTupleIterator()
  mti.mt = mt
  mti.node = mtn
  return mti
}

func initMultiTuple() {
  MultiTupleType = RegisterType(reflect.TypeOf((*MultiTuple)(nil)).Elem())
  MultiTupleIteratorType = RegisterType(reflect.TypeOf((*MultiTupleIterator)(nil)).Elem())
}

func initMultiTupleCoca() {
}
