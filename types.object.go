package generic

import (
  "reflect"
)

type Object struct {
  type_ *Type
}

func (o *Object) GetType() *Type {
  return o.type_
}

func (o *Object) SetType(t *Type) {
  if o.type_ == nil {
    o.type_ = t
  } else {
    panic("type can be set only once")
  }
}

func (o *Object) GetReflectType() reflect.Type {
  return o.type_.reflectType
}

type ObjectIf interface {
  GetType() *Type
  GetReflectType() reflect.Type
}
