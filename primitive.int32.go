package generic

import (
  "reflect"
  "strconv"
)

var Type_int32 *Type
var Int32Type *Type

var TypeInt32 reflect.Type

type Int32 struct {
  Object
  val int32
}

func NewInt32(ii int32) *Int32 {
  return new(Int32).Init(ii)
}

func (i *Int32) Init(ii int32) *Int32 {
  i.type_ = Int32Type
  i.val = ii
  return i
}

func (t *Int32) GetValue() int32 {
  return t.val
}

func (t *Int32) SetValue(i *Int32) {
  t.val = i.val;
}

func (t *Int32) Clone() interface{} {
  return NewInt32(t.val)
}

func (t *Int32) Compare(i interface{}) int {
  return Compare_Int32_Int32(t, i.(*Int32))
}

func Compare_Int32_Int32(a interface{}, b interface{}) int {
  aa := a.(*Int32).val
  bb := b.(*Int32).val
  if aa < bb {
    return -1
  } else if aa > bb {
    return 1
  } else {
    return 0
  }
}

func (t *Int32) ToString() *String {
  return NewString(strconv.Itoa(int(t.val)))
}

func Convert_Int32_string(a interface{}) interface{} {
  return Convert_int32_string(a.(*Int32).val)
}

func Convert_int32_string(a interface{}) interface{} {
  return strconv.Itoa(int(a.(int32)))
}

func Compare_int32_int32(a interface{}, b interface{}) int {
  A := a.(int32)
  B := b.(int32)
  if A < B {
    return -1
  } else if A > B {
    return 1
  } else {
    return 0
  }
}

func initInt32() {
  Type_int32 = RegisterType(reflect.TypeOf((*int32)(nil)).Elem())
  Int32Type = RegisterType(reflect.TypeOf((*Int32)(nil)).Elem())
}

func initInt32Coca() {
  RegisterConverter(Type_int32, Type_string, Convert_int32_string)
  RegisterConverter(Int32Type, Type_string, Convert_Int32_string)
  RegisterComparator(Type_int32, Type_int32, Compare_int32_int32)
  RegisterComparator(Int32Type, Int32Type, Compare_Int32_Int32)
}
