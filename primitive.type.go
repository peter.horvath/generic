package generic

import (
  "reflect"
  "sync"

  "avltree"
)

type Type struct {
  Object
  id int
  hash int32
  name string
  reflectType reflect.Type
}

var typeById avltree.AvlTree
var typeByHash avltree.AvlTree
var typeByName avltree.AvlTree
var nextTypeId = 1
var typeLock sync.RWMutex

var TypeType *Type = nil
//var Type_reflectType *Type

func (t *Type) GetId() int {
  return t.id
}

func (t *Type) GetHash() int32 {
  return t.hash
}

func (t *Type) GetName() string {
  return t.name
}

func RegisterType(t reflect.Type) *Type {
  r := new(Type)

  // registering TypeType, it is yet nil - post-fixed in initType
  r.type_ = TypeType
  r.name = GetTypeName(t)
  r.hash = int32(GetTypeHash(t) & 0x7fffffff)
  r.reflectType = t

  typeLock.Lock()
  r.id = nextTypeId
  nextTypeId++
  typeById.Add(r)
  typeByHash.Add(r)
  typeByName.Add(r)
  typeLock.Unlock()

  return r;
}

func (t *Type) Unregister() {
  typeLock.Lock()
  typeById.Remove(t)
  typeByHash.Remove(t)
  typeByName.Remove(t)
  cocaUnregisterTypeByHash(t.hash)
  typeLock.Unlock()
}

func Compare_Type_Type_byId(a interface{}, b interface{}) int {
  return a.(*Type).id - b.(*Type).id
}

func Compare_Type_Type_byName(a interface{}, b interface{}) int {
  return Compare_strings(a.(*Type).name, b.(*Type).name)
}

func Compare_Type_Type_byHash(a interface{}, b interface{}) int {
  return int(a.(*Type).hash - b.(*Type).hash)
}

func Compare_Type_Type_byReflectType(a interface{}, b interface{}) int {
  return Compare_strings(GetTypeName(a.(*Type).reflectType), GetTypeName(b.(*Type).reflectType))
}

func Convert_Type_reflectType(a interface{}) interface{} {
  return a.(Type).type_
}

func GetTypeById(id int) *Type {
  var t Type
  t.id = id
  typeLock.RLock()
  r := typeById.Find(&t)
  typeLock.RUnlock()
  if r == nil {
    return nil
  } else {
    return r.GetValue().(*Type)
  }
}

func GetTypeByHash(hash int32) *Type {
  var t Type
  t.hash = hash
  typeLock.RLock()
  r := typeByHash.Find(&t)
  typeLock.RUnlock()
  if r == nil {
    return nil
  } else {
    return r.GetValue().(*Type)
  }
}

func GetTypeByName(name string) *Type {
  var t Type
  t.name = name
  typeLock.RLock()
  r := typeByName.Find(&t)
  typeLock.RUnlock()
  if r == nil {
    return nil
  } else {
    return r.GetValue().(*Type)
  }
}

func GetTypeByReflectType(type_ reflect.Type) *Type {
  return GetTypeByHash(GetTypeHash(type_))
}

func GetTypeOf(a interface{}) *Type {
  if v, ok := a.(Object); ok {
    return v.type_
  } else {
    r := GetTypeByReflectType(reflect.TypeOf(a))
    if r == nil {
      panic ("type not registered: " + GetTypeName(reflect.TypeOf(r)))
    } else {
      return r
    }
  }
}

func initType() {
  typeLock.Lock()
  typeById.Init(Compare_Type_Type_byId, 0)
  typeByName.Init(Compare_Type_Type_byName, 0)
  typeByHash.Init(Compare_Type_Type_byHash, 0)
  typeLock.Unlock()
  //Type_reflectType = RegisterType(reflect.Type)
  TypeType = RegisterType(reflect.TypeOf((*Type)(nil)).Elem())
  TypeType.type_ = TypeType
}

func initTypeCoca() {
  RegisterComparator(TypeType, TypeType, Compare_Type_Type_byHash)
}
