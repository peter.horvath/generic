package generic

type Builder interface {
  Add(v interface{}) interface{}
  Build() interface{}
  IsEmpty() bool
}

type StringBuilder struct {
  l LinkedList
}

func NewStringBuilder() *StringBuilder {
  return new(StringBuilder).Init()
}

func (sb *StringBuilder) Init() *StringBuilder {
  sb.l.Init()
  return sb
}

func (sb *StringBuilder) Add(v string) *StringBuilder {
  sb.l.Add(v)
  return sb
}

func (sb *StringBuilder) Build() string {
  // TODO
  return ""
}

func (sb *StringBuilder) IsEmpty() bool {
  return sb.l.IsEmpty()
}
