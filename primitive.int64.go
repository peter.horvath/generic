package generic

import (
  "strconv"
  "reflect"
)

var Type_int64 *Type
var Int64Type *Type

type Int64 struct {
  Object
  val int64
}

func NewInt64(ii int64) *Int64 {
  return new(Int64).Init(ii)
}

func (i *Int64) Init(ii int64) *Int64 {
  i.type_ = Int64Type
  i.val = ii
  return i
}

func (t *Int64) GetValue() int64 {
  return t.val
}

func (t *Int64) SetValue(i *Int64) {
  t.val = i.val;
}

func (t *Int64) Clone() interface{} {
  return NewInt64(t.val)
}

func (t *Int64) ToString() string {
  return Convert_int64_string(t.val).(string)
}

func (t *Int64) Compare(i interface{}) int {
  return Compare_Int64_Int64(t, i.(*Int64))
}

func Compare_Int64_Int64(a interface{}, b interface{}) int {
  aa := a.(*Int64).val
  bb := b.(*Int64).val
  if aa < bb {
    return -1
  } else if aa > bb {
    return 1
  } else {
    return 0
  }
}

func Convert_int64_string(a interface{}) interface{} {
  return strconv.FormatInt(a.(int64), 10)
}

func Convert_Int64_string(a interface{}) interface{} {
  return Convert_int64_string(a.(*Int64).val)
}

func Compare_int64_int64(a interface{}, b interface{}) int {
  A := a.(int64)
  B := b.(int64)
  if A < B {
    return -1
  } else if A > B {
    return 1
  } else {
    return 0
  }
}

func initInt64() {
  Type_int64 = RegisterType(reflect.TypeOf((*int64)(nil)).Elem())
  Int64Type = RegisterType(reflect.TypeOf((*Int64)(nil)).Elem())
}

func initInt64Coca() {
  RegisterComparator(Type_int64, Type_int64, Compare_int64_int64)
  RegisterComparator(Int64Type, Int64Type, Compare_Int64_Int64)
  RegisterConverter(Type_int64, Type_string, Convert_int64_string)
  RegisterConverter(Int64Type, Type_string, Convert_Int64_string)
}
