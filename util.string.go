package generic

import (
  "bytes"
  "encoding/json"
  "hash/crc32"
)

func JsonPrettyPrint(in string) string {
  var out bytes.Buffer
  err := json.Indent(&out, []byte(in), "", "\t")
  if err != nil {
    return in
  }
  return out.String()
}

func ConcatStringSlice(in []string) string {
  var resultLen int64 = 0
  var i int64
  for i = 0; i < int64(len(in)); i++ {
    resultLen += int64(len(in[i]))
  }
  res := make([]byte, resultLen)
  var l int64 = 0
  for i = 0; i < int64(len(in)); i++ {
    copy(res[l:], in[i])
    l += int64(len(in[i]))
  }

  return string(res)
}

func ConcatStrings(in ...string) string {
  return ConcatStringSlice(in)
}

func JoinStringSlice(in []string, sep string) string {
  if len(in) == 0 {
    return ""
  }
  slice := make([]string, 2 * len(in) - 1)
  for i := 0; i < len(in); i++ {
    slice[2*i] = in[i]
    if i < len(in) -1 {
      slice[2*i+1] = sep
    }
  }
  return ConcatStringSlice(slice)
}

func JoinStrings(in ...string) string {
  sep := in[len(in)-1]
  stringArr := in[0:len(in)-1]
  return JoinStringSlice(stringArr, sep)
}

func Crc32(s string) uint32 {
  return crc32.ChecksumIEEE([]byte(s))
}
