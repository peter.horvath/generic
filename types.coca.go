// Generic compare and casting

package generic

import (
  "avltree"
)

var cocaMap avltree.AvlTree

type Comparator func(a interface{}, b interface{}) int
type Converter func(a interface{}) interface{}

type coca struct {
  type1, type2 *Type
  hash int
  comparator Comparator
  converter Converter
}

func cocaComparator(a interface{}, b interface{}) int {
  return a.(*coca).hash - b.(*coca).hash
}

func (c *coca) Init(a *Type, b *Type) *coca {
  c.type1 = a
  c.type2 = b
  c.hash = int(a.GetHash() ^ (b.GetHash() + 0x2f2f3bd9)) & 0x7fffffff
  return c
}

func (c *coca) getComparator() Comparator {
  return c.comparator
}

func (c *coca) setComparator(cf Comparator) {
  c.comparator = cf
}

func (c *coca) getConverter() Converter {
  return c.converter
}

func (c *coca) setConverter(cf Converter) {
  c.converter = cf
}

func cocaFind(a *Type, b *Type) *coca {
  var c coca
  c.Init(a, b)
  cNode := cocaMap.Find(&c)
  if cNode == nil {
    return nil
  } else {
    return cNode.GetValue().(*coca)
  }
}

func cocaCreateFind(a *Type, b *Type) *coca {
  c := new(coca).Init(a, b)
  cNode := cocaMap.Find(c)
  if cNode == nil {
    cocaMap.Add(c)
    return c
  } else {
    return cNode.GetValue().(*coca)
  }
}

func (c *coca) removeIfEmpty() {
  if c.comparator == nil && c.converter == nil {
    cocaMap.Remove(c)
  }
}

func GetComparator(a *Type, b *Type) Comparator {
  typeLock.RLock()

  c := cocaFind(a, b)
  var r Comparator = nil
  if c != nil {
    r = c.comparator
  }

  typeLock.RUnlock()
  return r
}

func GetConverter(a *Type, b *Type) Converter {
  typeLock.RLock()

  c := cocaFind(a, b)
  var r Converter = nil
  if c != nil {
    r = c.converter
  }

  typeLock.RUnlock()
  return r
}

func RegisterComparator(a *Type, b *Type, cf Comparator) {
  typeLock.Lock()
  c := cocaCreateFind(a, b)
  c.setComparator(cf)
  typeLock.Unlock()
}

func RegisterConverter(a *Type, b *Type, cf Converter) {
  typeLock.Lock()
  c := cocaCreateFind(a, b)
  c.setConverter(cf)
  typeLock.Unlock()
}

func UnregisterComparator(a *Type, b *Type) {
  typeLock.Lock()
  c := cocaFind(a, b)
  if c != nil {
    c.comparator = nil
    c.removeIfEmpty()
  }
  typeLock.Unlock()
}

func UnregisterConverter(a *Type, b *Type) {
  typeLock.Lock()
  c := cocaFind(a, b)
  if c != nil {
    c.converter = nil
    c.removeIfEmpty()
  }
  typeLock.Unlock()
}

func cocaUnregisterTypeByHash(hash int32) {
  typeLock.Lock()
  cnode := cocaMap.GetFirst()
  for {
    c := cnode.GetValue().(*coca)
    nxt := cnode.GetNext()
    if (c.type1.hash == hash) || (c.type2.hash == hash) {
      cocaMap.RemoveNode(cnode)
    }
    if nxt == nil {
      break
    }
    cnode = nxt
  }
  typeLock.Unlock()
}

func Compare(a interface{}, b interface{}) int {
  ta := GetTypeOf(a)
  tb := GetTypeOf(b)
  c := GetComparator(ta, tb)
  if c == nil {
    panic("can't compare " + ta.name + " to " + tb.name)
  } else {
    return c(a, b)
  }
}

func Is(a interface{}, t *Type) bool {
  return GetTypeOf(a) == t
}

func As(a interface{}, t *Type) interface{} {
  aType := GetTypeOf(a)
  converter := GetConverter(aType, t)
  /*
  fmt.Printf("As: a type name: %s a type kind: %s t.name: %s t kind: %s \n", reflect.TypeOf(a).Name(), reflect.TypeOf(a).Kind().String(), t.Name(), t.Kind().String())
  if aType.Kind() == reflect.Ptr {
    fmt.Printf("    a ptr type name: %s, kind: %s\n", reflect.TypeOf(a).Elem().Name(), reflect.TypeOf(a).Elem().Kind().String())
    fmt.Printf("    a ptr value name: %s, kind: %s\n", reflect.TypeOf(reflect.ValueOf(a).Elem()).Name(), reflect.TypeOf(reflect.ValueOf(a).Elem()).Kind().String())
  }*/
  if converter == nil {
    panic("no registered converter for " + aType.name + " to " + t.name)
  } else {
    return converter(a)
  }
}

func initCoca() {
  typeLock.Lock()
  cocaMap.Init(cocaComparator, 0);
  typeLock.Unlock()
}
