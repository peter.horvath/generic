// TODO

package generic

import (
  "reflect"
)

var ArrayListType *Type
var ArrayListIteratorType *Type

type ArrayList struct {
  Object
  slice []interface{}
  size int64
}

type ArrayListIterator struct {
  Object
  list *ArrayList
  n int64
}

func NewArrayList() *ArrayList {
  return new(ArrayList).Init()
}

func (l *ArrayList) Init() *ArrayList {
  l.type_ = ArrayListType
  l.slice = make([]interface{}, 0)
  l.size = 0
  return l
}

func (l *ArrayList) At(n int64) *ArrayListIterator {
  ali := NewArrayListIterator()
  ali.list = l
  if (n < 0) || (n > l.size) {
    ali.n = -1
  } else {
    ali.n = n
  }
  return ali
}

func (l *ArrayList) GetFirst() *ArrayListIterator {
  ali := NewArrayListIterator()
  ali.list = l
  if !l.IsEmpty() {
    ali.n = 0
  }
  return ali
}

func (l *ArrayList) GetLast() *ArrayListIterator {
  ali := NewArrayListIterator()
  ali.list = l
  if !l.IsEmpty() {
    ali.n = l.size - 1
  }
  return ali
}

func (l *ArrayList) GetSize() int64 {
  return l.size
}

func (l *ArrayList) IsEmpty() bool {
  return l.size == 0
}

func (l *ArrayList) Join(sep string) string {
  if l.size == 0 {
    return ""
  } else {
    ss := make([]string, l.size)
    for i := int64(0); i < l.size; i++ {
      ss[i] = As(l.slice[i], Type_string).(string)
    }
    return JoinStringSlice(ss, sep)
  }
}

func (l *ArrayList) SetSize(size int64) *ArrayList {
  if l.size == 0 {
    if size == 0 {
      return l
    } else {
      l.slice = make([]interface{}, size)
      l.size = size
      return l
    }
  } else {
    if size == 0 {
      l.slice = nil
      l.size = 0
      return l
    } else {
      newslice := make([]interface{}, size)
      max := size
      if max > l.size {
        max = l.size
      }
      for i := int64(0); i < max; i++ {
        newslice[i] = l.slice[i]
      }
      l.slice = newslice
      l.size = size
      return l
    }
  }
}

func (l *ArrayList) ToString() string {
  return ConcatStrings("[", l.Join(","), "]")
}

func NewArrayListIterator() *ArrayListIterator {
  return new(ArrayListIterator).Init()
}

func (ali *ArrayListIterator) Init() *ArrayListIterator {
  ali.type_ = ArrayListIteratorType
  ali.list = nil
  ali.n = -1
  return ali
}

func (ali *ArrayListIterator) GetArrayList() *ArrayList {
  return ali.list
}

func (ali *ArrayListIterator) GetIdx() int64 {
  if ali.IsNull() {
    return -1
  } else {
    return ali.n
  }
}

func (ali *ArrayListIterator) GetNext() *ArrayListIterator {
  nali := ali.Clone()
  if nali.IsNull() {
    return nali
  }
  nali.n++
  return nali
}

func (ali *ArrayListIterator) GetPrev() *ArrayListIterator {
  pali := ali.Clone()
  if pali.IsNull() {
    return pali
  }
  pali.n--
  return pali
}

func (ali *ArrayListIterator) GetValue() interface{} {
  if ali.IsNull() {
    return nil
  } else {
    return ali.list.slice[ali.n]
  }
}

func (ali *ArrayListIterator) IsFirst() bool {
  return !ali.IsNull() && (ali.n == 0)
}

func (ali *ArrayListIterator) IsLast() bool {
  return !ali.IsNull() && (ali.n == ali.list.size - 1)
}

func (ali *ArrayListIterator) IsNull() bool {
  return (ali.list != nil) && (ali.n >= 0) && (ali.n < ali.list.size)
}

func (ali *ArrayListIterator) SetArrayList(l *ArrayList) *ArrayListIterator {
  ali.list = l
  ali.n = 0
  if ali.IsNull() {
    ali.n = -1
  }
  return ali
}

func (ali *ArrayListIterator) SetValue(value interface{}) *ArrayListIterator {
  ali.list.slice[ali.n] = value
  return ali
}

func (ali *ArrayListIterator) Step() *ArrayListIterator {
  if !ali.IsNull() {
    ali.n++
  }
  return ali
}

func (ali *ArrayListIterator) StepBack() *ArrayListIterator {
  if !ali.IsNull() {
    ali.n--
  }
  return ali
}

func (ali *ArrayListIterator) Clone() *ArrayListIterator {
  nali := NewArrayListIterator()
  nali.list = ali.list
  nali.n = ali.n
  return nali
}

func initArrayList() {
  ArrayListType = RegisterType(reflect.TypeOf((*ArrayList)(nil)).Elem())
  ArrayListIteratorType = RegisterType(reflect.TypeOf((*ArrayListIterator)(nil)).Elem())
}

func initArrayListCoca() {
}
