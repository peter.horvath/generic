package generic

import (
  "reflect"

  "avltree"
)

var TreeListType *Type
var TreeListIteratorType *Type

type TreeList struct {
  Object
  tree *avltree.AvlTree
}

type TreeListIterator struct {
  Object
  treeList *TreeList
  avlNode *avltree.AvlNode
}

func NewTreeList() *TreeList {
  return new(TreeList).Init()
}

func (tl *TreeList) Init() *TreeList {
  tl.type_ = TreeListType
  tl.tree = avltree.NewAvlTree(nil, 0)
  return tl
}

func (tl *TreeList) Add(v interface{}) *TreeListIterator {
  return tl.AddLast(v)
}

func (tl *TreeList) AddFirst(v interface{}) *TreeListIterator {
  tli := NewTreeListIterator().SetTreeList(tl)
  tli.avlNode = tl.tree.UnsortedAddFirst(v)
  return tli
}

func (tl *TreeList) AddLast(v interface{}) *TreeListIterator {
  tli := NewTreeListIterator().SetTreeList(tl)
  tli.avlNode = tl.tree.UnsortedAddLast(v)
  return tli
}

func (tl *TreeList) At(n int64) *TreeListIterator {
  tli := NewTreeListIterator().SetTreeList(tl)
  tli.avlNode = tl.tree.NodeAt(n)
  return tli
}

func (tl *TreeList) GetFirst() *TreeListIterator {
  tli := NewTreeListIterator().SetTreeList(tl)
  tli.avlNode = tl.tree.GetFirst()
  return tli
}

func (tl *TreeList) GetLast() *TreeListIterator {
  tli := NewTreeListIterator().SetTreeList(tl)
  tli.avlNode = tl.tree.GetLast()
  return tli
}

func (tl *TreeList) GetSize() int64 {
  return tl.tree.GetSize()
}

func (tl *TreeList) Insert(idx int64, v interface{}) *TreeListIterator {
  tli := NewTreeListIterator().SetTreeList(tl)
  tli.avlNode = tl.tree.UnsortedInsert(idx, v)
  return tli
}

func (tl *TreeList) IsEmpty() bool {
  return tl.tree.IsEmpty()
}

func (tl *TreeList) Join(sep string) string {
  if tl.IsEmpty() {
    return ""
  }

  slice := make([]string, tl.GetSize())

  var n int64 = 0
  for i := tl.GetFirst(); !i.IsNull(); i.Step() {
    slice[n] = As(i.GetValue(), Type_string).(string)
    n++
  }
  return JoinStringSlice(slice, sep)
}

func (tl *TreeList) Pop() interface{} {
  if tl.IsEmpty() {
    return nil
  }
  n := tl.tree.GetFirst()
  tl.tree.RemoveNode(n)
  return n.GetValue()
}

func (tl *TreeList) PopLast() interface{} {
  if tl.IsEmpty() {
    return nil
  }
  n := tl.tree.GetLast()
  tl.tree.RemoveNode(n)
  return n.GetValue()
}

func (tl *TreeList) ToString() string {
  return ConcatStrings("[", tl.Join(","), "]")
}

func NewTreeListIterator() *TreeListIterator {
  return new(TreeListIterator).Init()
}

func (tli *TreeListIterator) Init() *TreeListIterator {
  tli.type_ = TreeListIteratorType
  return tli
}

func (tli *TreeListIterator) AddAfter(v interface{}) *TreeListIterator {
  ntli := NewTreeListIterator().SetTreeList(tli.treeList)
  ntli.avlNode = tli.treeList.tree.UnsortedAddAfter(tli.avlNode, v)
  return ntli
}

func (tli *TreeListIterator) AddBefore(v interface{}) *TreeListIterator {
  ntli := NewTreeListIterator().SetTreeList(tli.treeList)
  ntli.avlNode = tli.treeList.tree.UnsortedAddBefore(tli.avlNode, v)
  return ntli
}

func (tli *TreeListIterator) GetIdx() int64 {
  return tli.avlNode.GetIdx()
}

func (tli *TreeListIterator) GetNext() *TreeListIterator {
  ntli := NewTreeListIterator().SetTreeList(tli.treeList)
  ntli.avlNode = tli.avlNode.GetNext()
  return ntli
}

func (tli *TreeListIterator) GetPrev() *TreeListIterator {
  ntli := NewTreeListIterator().SetTreeList(tli.treeList)
  ntli.avlNode = tli.avlNode.GetNext()
  return ntli
}

func (tli *TreeListIterator) GetTreeList() *TreeList {
  return tli.treeList
}

func (tli *TreeListIterator) GetValue() interface{} {
  return tli.avlNode.GetValue()
}

func (tli *TreeListIterator) IsFirst() bool {
  return tli.avlNode == tli.treeList.tree.GetFirst()
}

func (tli *TreeListIterator) IsLast() bool {
  return tli.avlNode == tli.treeList.tree.GetLast()
}

func (tli *TreeListIterator) IsNull() bool {
  return tli.treeList == nil || tli.avlNode == nil
}

func (tli *TreeListIterator) Remove() *TreeListIterator {
  tli.treeList.tree.RemoveNode(tli.avlNode)
  return tli
}

func (tli *TreeListIterator) SetIdx(n int64) *TreeListIterator {
  value := tli.avlNode.GetValue()
  tli.Remove()
  tli.avlNode = tli.treeList.tree.UnsortedInsert(n, value)
  return tli
}

func (tli *TreeListIterator) SetTreeList(tl *TreeList) *TreeListIterator {
  if tl != tli.treeList {
    tli.treeList = tl
    tli.avlNode = tl.tree.GetFirst()
  }
  return tli
}

func (tli *TreeListIterator) SetValue(value interface{}) *TreeListIterator {
  tli.avlNode.UnsortedSetValue(value)
  return tli
}

func (tli *TreeListIterator) Step() *TreeListIterator {
  if tli.avlNode != nil {
    tli.avlNode = tli.avlNode.GetNext()
  }
  return tli
}

func (tli *TreeListIterator) StepBack() *TreeListIterator {
  if tli.avlNode != nil {
    tli.avlNode = tli.avlNode.GetPrev()
  }
  return tli
}

func initTreeList() {
  TreeListType = RegisterType(reflect.TypeOf((*TreeList)(nil)).Elem())
  TreeListIteratorType = RegisterType(reflect.TypeOf((*TreeListIterator)(nil)).Elem())
}

func initTreeListCoca() {
}
