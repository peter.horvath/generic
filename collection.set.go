package generic

import (
  "fmt"
  "reflect"

  "avltree"
)

var SetType *Type
var SetIteratorType *Type

type Set struct {
  Object
  tree *avltree.AvlTree
}

type SetIterator struct {
  Object
  set *Set
  avlNode *avltree.AvlNode
}

func NewSet() *Set {
  return new(Set).Init()
}

func (s *Set) Init() *Set {
  s.type_ = SetType
  s.tree = avltree.NewAvlTree(Compare, 0)
  return s
}

func (s *Set) Add(v interface{}) *Set {
  fmt.Printf("Set.Add(%s)\n", As(v, Type_string))
  s.tree.Add(v)
  return s
}

func (s *Set) At(n int64) *SetIterator {
  si := NewSetIterator()
  si.set = s
  si.avlNode = s.tree.NodeAt(n)
  return si
}

func (s *Set) Find(v interface{}) *SetIterator {
  si := NewSetIterator().SetSet(s)
  si.avlNode = s.tree.Find(v)
  return si
}

func (s *Set) GetComparator() Comparator {
  return Comparator(s.tree.GetComparator())
}

func (s *Set) GetFirst() *SetIterator {
  si := NewSetIterator().SetSet(s)
  si.avlNode = s.tree.GetFirst()
  return si
}

func (s *Set) GetLast() *SetIterator {
  si := NewSetIterator().SetSet(s)
  si.avlNode = s.tree.GetLast()
  return si
}

func (s *Set) GetSize() int64 {
  return s.tree.GetSize()
}

func (s *Set) Has(v interface{}) bool {
  return s.tree.Has(v)
}

func (s *Set) IsEmpty() bool {
  return s.tree.IsEmpty()
}

func (s *Set) Join(sep string) string {
  if s.IsEmpty() {
    return ""
  }

  slice := make([]string, s.GetSize())

  var n int64 = 0
  for i := s.GetFirst(); !i.IsNull(); i.Step() {
    slice[n] = As(i.GetValue(), Type_string).(string)
    n++
  }
  return JoinStringSlice(slice, sep)
}

func (s *Set) Pop() interface{} {
  if s.IsEmpty() {
    return nil
  }
  v := s.GetFirst()
  v.Remove()
  return v.GetValue()
}

func (s *Set) PopLast() interface{} {
  if s.IsEmpty() {
    return nil
  }
  v := s.GetLast()
  v.Remove()
  return v.GetValue()
}

func (s *Set) Remove(v interface{}) *Set {
  i := s.Find(v)
  if i.IsNull() {
    panic ("not in set, can not remove")
  }
  i.Remove()
  return s
}

func (s *Set) SetComparator(c Comparator) *Set {
  s.tree.SetComparator(avltree.CompareFunc(c))
  return s
}

func (s *Set) ToString() string {
  return ConcatStrings("[", s.Join(","), "]")
}

func NewSetIterator() *SetIterator {
  return new(SetIterator).Init()
}

func (si *SetIterator) Init() *SetIterator {
  si.type_ = SetIteratorType
  return si
}

func (si *SetIterator) GetIdx() int64 {
  return si.avlNode.GetIdx()
}

func (si *SetIterator) GetNext() *SetIterator {
  nsi := NewSetIterator().SetSet(si.set)
  nsi.avlNode = si.avlNode.GetNext()
  return nsi
}

func (si *SetIterator) GetPrev() *SetIterator {
  nsi := NewSetIterator().SetSet(si.set)
  nsi.avlNode = si.avlNode.GetPrev()
  return nsi
}

func (si *SetIterator) GetSet() *Set {
  return si.set
}

func (si *SetIterator) GetValue() interface{} {
  return si.avlNode.GetValue()
}

func (si *SetIterator) IsFirst() bool {
  return si.avlNode == si.set.tree.GetFirst()
}

func (si *SetIterator) IsLast() bool {
  return si.avlNode == si.set.tree.GetLast()
}

func (si *SetIterator) IsNull() bool {
  return (si.set == nil) || (si.avlNode == nil)
}

func (si *SetIterator) Remove() *SetIterator {
  si.set.tree.RemoveNode(si.avlNode)
  si.set = nil
  return si
}

func (si *SetIterator) SetSet(set *Set) *SetIterator {
  if si.set != set {
    si.set = set
    si.avlNode = set.tree.GetFirst()
  }
  return si
}

func (si *SetIterator) SetValue(value interface{}) *SetIterator {
  si.set.tree.RemoveNode(si.avlNode)
  si.avlNode = si.set.tree.Add(value)
  return si
}

func (si *SetIterator) Step() *SetIterator {
  if si.avlNode != nil {
    si.avlNode = si.avlNode.GetNext()
  }
  return si
}

func (si *SetIterator) StepBack() *SetIterator {
  if si.avlNode != nil {
    si.avlNode = si.avlNode.GetPrev()
  }
  return si
}

func initSet() {
  SetType = RegisterType(reflect.TypeOf((*Set)(nil)).Elem())
  SetIteratorType = RegisterType(reflect.TypeOf((*SetIterator)(nil)).Elem())
}

func initSetCoca() {
}
