package generic

import (
  "testing"
  "fmt"
)

func (m *Map) Dump() {
  str := m.ToString()
  fmt.Println(JsonPrettyPrint(str))
}

func noTestMap(test *testing.T) {
  m := NewMap()
  for i := 0; i < 10; i++ {
    fmt.Printf("added %d\n", i)
    m.Set(i, i*i)
    //m.Set(NewInt32(int32(i)), NewInt32(int32(i*i)))
  }
  m.Dump()
}
