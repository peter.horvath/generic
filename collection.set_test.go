package generic

import (
  "fmt"
  "testing"
)

func (s *Set) Dump() {
  fmt.Println("1")
  str := s.ToString()
  fmt.Println("2")
  fmt.Println("3")
  fmt.Println(JsonPrettyPrint(str))
  fmt.Println("4")
}

func noTestSet(test *testing.T) {
  s := NewSet().SetComparator(Compare)
  for i := 0; i < 10; i++ {
    fmt.Printf("added %d\n", i)
    s.Add(NewInt32(int32(i)))
  }
  s.Dump()
}
