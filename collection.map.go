package generic

import (
  "reflect"
  //"fmt"

  "avltree"
)

var MapType *Type
var MapIteratorType *Type

type Map struct {
  Object
  tree *avltree.AvlTree
  keyComparator Comparator
  valueComparator Comparator
  pairComparator Comparator
  keyToPairComparator Comparator
}

type MapIterator struct {
  Object
  m *Map
  avlNode *avltree.AvlNode
}

func NewMap() *Map {
  return new(Map).Init()
}

func (m *Map) Init() *Map {
  m.type_ = MapType
  m.keyComparator = Compare
  m.valueComparator = Compare
  m.tree = avltree.NewAvlTree(nil, 0)
  m.refreshComparators()
  return m
}

func (m *Map) Add(p *Pair) *MapIterator {
  mi := NewMapIterator().SetMap(m)
  mi.avlNode = m.tree.UnsortedAddLast(p)
  return mi
}

func (m *Map) At(n int64) *MapIterator {
  mi := NewMapIterator().SetMap(m)
  mi.avlNode = m.tree.NodeAt(n)
  return mi
}

func (m *Map) Find(key interface{}) *MapIterator {
  avlNode := m.tree.FindByCompareFunc(key, m.compareKeyToPair)
  if avlNode == nil {
    mi := NewMapIterator()
    mi.m = m
    return mi
  } else {
    mi := NewMapIterator().SetMap(m)
    mi.avlNode = avlNode
    return mi
  }
}

func (m *Map) Get(key interface{}) interface{} {
  return m.tree.FindByCompareFunc(key, m.compareKeyToPair).GetValue().(*Pair).value
}

func (m *Map) GetFirst() *MapIterator {
  mi := NewMapIterator().SetMap(m)
  mi.avlNode = m.tree.GetFirst()
  return mi
}

func (m *Map) GetKeyComparator() Comparator {
  return m.keyComparator
}

func (m *Map) GetKeySet() *Set {
  s := NewSet().SetComparator(m.keyComparator)
  for mi := m.GetFirst(); !mi.IsNull(); mi.Step() {
    s.Add(mi.GetKey())
  }
  return s
}

func (m *Map) GetLast() *MapIterator {
  mi := NewMapIterator().SetMap(m)
  mi.avlNode = m.tree.GetLast()
  return mi
}

func (m *Map) GetPairSet() *Set {
  s := NewSet().SetComparator(m.pairComparator)
  for mi := m.GetFirst(); !mi.IsNull(); mi.Step() {
    //fmt.Printf("Map.GetPairSet(): type of value: %s kind of value: %s\n", reflect.TypeOf(mi.GetValue()).Name(), reflect.TypeOf(mi.GetValue()).Kind().String())
    //fmt.Printf("Map.GetPairSet(): type of value elem: %s kind of value elem: %s\n", reflect.TypeOf(mi.GetValue()).Elem().Name(), reflect.TypeOf(mi.GetValue()).Elem().Kind().String())
    s.Add(mi.GetPair())
  }
  return s
}

func (m *Map) GetSize() int64 {
  return m.tree.GetSize()
}

func (m *Map) GetValueComparator() Comparator {
  return m.valueComparator
}

func (m *Map) GetValueSet() *Set {
  s := NewSet().SetComparator(m.valueComparator)
  for mi := m.GetFirst(); !mi.IsNull(); mi.Step() {
    s.Add(mi.GetValue())
  }
  return s
}

func (m *Map) Has(key interface{}) bool {
  return m.tree.HasByCompareFunc(key, m.compareKeyToPair)
}

func (m *Map) IsEmpty() bool {
  return m.tree.IsEmpty()
}

func (m *Map) Pop() *Pair {
  if (m.IsEmpty()) {
    return nil
  }
  mi := m.GetFirst()
  m.tree.RemoveNode(mi.avlNode)
  return mi.GetValue().(*Pair)
}

func (m *Map) PopLast() *Pair {
  if (m.IsEmpty()) {
    return nil
  }
  mi := m.GetLast()
  m.tree.RemoveNode(mi.avlNode)
  return mi.GetValue().(*Pair)
}

func (m *Map) Set(key interface{}, value interface{}) *Map {
  if m.Has(key) {
    m.Unset(key)
  }
  m.tree.Add(NewPair(key, value))
  return m
}

func (m *Map) SetKeyComparator(c Comparator) {
  m.keyComparator = c
  m.refreshComparators()
}

func (m *Map) SetValueComparator(c Comparator) {
  m.valueComparator = c
  m.refreshComparators()
}

func (m *Map) Unset(key interface{}) *Map {
  if m.Has(key) {
    mi := m.Find(key)
    m.tree.RemoveNode(mi.avlNode)
  }
  return m
}

func (m *Map) ToString() string {
  r := ConcatStrings("{", m.GetPairSet().Join(","), "}")
  return r
}

func NewMapIterator() *MapIterator {
  return new(MapIterator).Init()
}

func (mi *MapIterator) Init() *MapIterator {
  mi.type_ = MapIteratorType
  return mi
}

func (mi *MapIterator) GetIdx() int64 {
  return mi.avlNode.GetIdx()
}

func (mi *MapIterator) GetKey() interface{} {
  return mi.avlNode.GetValue().(*Pair).value
}

func (mi *MapIterator) GetMap() *Map {
  return mi.m
}

func (mi *MapIterator) GetNext() *MapIterator {
  nmi := NewMapIterator().SetMap(mi.m)
  nmi.avlNode = mi.avlNode.GetNext()
  return nmi
}

func (mi *MapIterator) GetPair() *Pair {
  return mi.avlNode.GetValue().(*Pair)
}

func (mi *MapIterator) GetPrev() *MapIterator {
  nmi := NewMapIterator().SetMap(mi.m)
  nmi.avlNode = mi.avlNode.GetPrev()
  return nmi
}

func (mi *MapIterator) GetValue() interface{} {
  return mi.avlNode.GetValue().(*Pair).value
}

func (mi *MapIterator) IsFirst() bool {
  return mi.avlNode == mi.m.tree.GetFirst()
}

func (mi *MapIterator) IsLast() bool {
  return mi.avlNode == mi.m.tree.GetLast()
}

func (mi *MapIterator) IsNull() bool {
  return (mi.m == nil) || (mi.avlNode == nil)
}

func (mi *MapIterator) Remove() *MapIterator {
  mi.m.tree.RemoveNode(mi.avlNode)
  return mi
}

func (mi *MapIterator) SetKey(key interface{}) *MapIterator {
  p := mi.avlNode.GetValue().(*Pair)
  mi.m.tree.RemoveNode(mi.avlNode)
  p.key = key
  mi.avlNode = mi.m.tree.Add(p)
  return mi
}

func (mi *MapIterator) SetMap(m *Map) *MapIterator {
  if mi.m != m {
    mi.m = m
    mi.avlNode = m.tree.GetFirst()
  }
  return mi
}

func (mi *MapIterator) SetValue(value interface{}) *MapIterator {
  p := mi.avlNode.GetValue().(*Pair)
  mi.m.tree.RemoveNode(mi.avlNode)
  p.value = value
  mi.avlNode = mi.m.tree.Add(p)
  return mi
}

func (mi *MapIterator) Step() *MapIterator {
  if mi.avlNode != nil {
    mi.avlNode = mi.avlNode.GetNext()
  }
  return mi
}

func (mi *MapIterator) StepBack() *MapIterator {
  if mi.avlNode != nil {
    mi.avlNode = mi.avlNode.GetPrev()
  }
  return mi
}

func (m *Map) comparePairs(p1 interface{}, p2 interface{}) int {
  pair1 := p1.(*Pair)
  pair2 := p2.(*Pair)
  r := m.keyComparator(p1, p2)
  if r != 0 {
    return r
  } else {
    return m.valueComparator(pair1.value, pair2.value)
  }
}

func (m *Map) compareKeyToPair(key interface{}, pair interface{}) int {
  return m.keyComparator(key, pair.(*Pair).key)
}

func (m *Map) refreshComparators() {
  m.pairComparator = func (p1 interface{}, p2 interface{}) int {
    return m.comparePairs(p1, p2)
  }
  m.keyToPairComparator = func (key interface{}, pair interface{}) int {
    return m.compareKeyToPair(key, pair)
  }
  m.tree.SetComparator(avltree.CompareFunc(m.pairComparator))
}

func initMap() {
  MapType = RegisterType(reflect.TypeOf((*Map)(nil)).Elem())
  MapIteratorType = RegisterType(reflect.TypeOf((*MapIterator)(nil)).Elem())
}

func initMapCoca() {
}
