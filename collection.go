package generic

type CollectionIf interface {
  Add(interface{}) *IteratorIf
  AddFirst(interface{}) *IteratorIf
  AddLast(interface{}) *IteratorIf
  At(int64) *IteratorIf
  Find(interface{}) *IteratorIf
  GetFirst() *IteratorIf
  GetLast() *IteratorIf
  GetSize() int64
  Has(interface{}) *IteratorIf
  Insert(int64, interface{}) *IteratorIf
  IsEmpty() bool
  Join(interface{}, string) string
  SetSize(int64) *CollectionIf
  Pop() interface{}
  PopLast() interface{}
  ToString(interface{}) *IteratorIf
}

// generic collection - can be anything
/*
struct Collection {
  CollectionIf
  value *CollectionIf
}
*/

type IteratorIf interface {
  AddAfter(interface{}) *IteratorIf
  AddBefore(interface{}) *IteratorIf
  GetIdx() int64
  GetCollection() *CollectionIf
  GetNext() *IteratorIf
  GetPrev() *IteratorIf
  GetValue() interface{}
  IsFirst() bool
  IsLast() bool
  IsNull() bool
  Remove() *IteratorIf
  SetCollection(*CollectionIf) *IteratorIf
  SetIdx(int64) *IteratorIf
  SetValue(interface{}) *IteratorIf
  Step() *IteratorIf
  StepBack() *IteratorIf
}

/*
func (c *CollectionStruct) Append(cc *CollectionStruct) *Collection {
  return cc.Map(func(i Iterator) {
    c.Add(i.GetValue())
  })
}

func (c *CollectionStruct) Map(func (i Iterator)) *Collection {
  for i := c.GetFirst(); !i.IsNull(); i.Step() {
    m(i)
  }
  return c
}
*/

/*

Collection:

New, Init, Add, AddFirst, AddLast, IsEmpty, GetSize, SetSize, GetFirst, GetLast, Pop, PopLast, Find, Has, Join/ToString, AddAfter, AddBefore

No: RemoveByIter, Get/Set/Unset, Get/Set Comparator,  

Iterator:

New, Init, IsNull, Remove, GetValue, SetValue, GetNext, GetPrev, Step, StepBack, IsFirst, IsLast, GetIdx, SetIdx

*/
