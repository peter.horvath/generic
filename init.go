package generic

func init() {
  // init main core
  initType()
  initCoca()

  // init basic types
  initString()
  initBoolean()
  initInt()
  initInt32()
  initInt64()

  // init collection types
  initPair()
  initLinkedList()
  initArrayList()
  initTreeList()
  initSet()
  initMap()
  initMultiTuple()

  // init basic cocas
  initStringCoca()
  initBooleanCoca()
  initIntCoca()
  initInt32Coca()
  initInt64Coca()

  // init collection cocas
  initPairCoca()
  initLinkedListCoca()
  initArrayListCoca()
  initTreeListCoca()
  initSetCoca()
  initMapCoca()
  initMultiTupleCoca()
}
