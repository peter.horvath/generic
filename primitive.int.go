package generic

import (
  "strconv"
  "reflect"
)

type Int struct {
  Object
  val int
}

var Type_int *Type
var IntType *Type

func NewInt(i int) *Int {
  return new(Int).Init(i)
}

func (i *Int) Init(ii int) *Int {
  i.type_ = IntType
  i.val = ii
  return i
}

func (t *Int) GetValue() int {
  return t.val
}

func (t *Int) SetValue(i *Int) {
  t.val = i.val;
}

func (t *Int) Clone() interface{} {
  return NewInt(t.val)
}

func (t *Int) Compare(i interface{}) int {
  return CompareInt(t, i.(*Int))
}

func CompareInt(a interface{}, b interface{}) int {
  if (a.(Int).val < b.(Int).val) {
    return -1
  } else if (a.(Int).val > b.(Int).val) {
    return 1
  } else {
    return 0
  }
}

func (t *Int) ToString() *String {
  return NewString(strconv.Itoa(int(t.val)))
}

func Compare_int(a int, b int) int {
  if a < b {
    return -1
  } else if a > b {
    return 1
  } else {
    return 0
  }
}

func Compare_int_int(a interface{}, b interface{}) int {
  return Compare_int(a.(int), b.(int))
}

func Convert_int_string(a interface{}) interface{} {
  return strconv.Itoa(a.(int))
}

func initInt() {
  Type_int = RegisterType(reflect.TypeOf((*int)(nil)).Elem())
  IntType = RegisterType(reflect.TypeOf((*Int)(nil)).Elem())
}

func initIntCoca() {
  RegisterComparator(Type_int, Type_int, Compare_int_int)
  RegisterConverter(Type_int, Type_string, Convert_int_string)
}
