package generic

import (
  "reflect"
)

var LinkedListType *Type
var LinkedListIteratorType *Type

type LinkedList struct {
  Object
  first *linkedListNode
  last *linkedListNode
  size int64
}

type linkedListNode struct {
  prev *linkedListNode
  next *linkedListNode
  value interface{}
}

type LinkedListIterator struct {
  Object
  list *LinkedList
  node *linkedListNode
}

func NewLinkedList() *LinkedList {
  return new(LinkedList).Init()
}

func (l *LinkedList) Init() *LinkedList {
  l.type_ = LinkedListType
  l.first = nil
  l.last = nil
  l.size = 0
  return l
}

func (l *LinkedList) Add(v interface{}) *LinkedListIterator {
  return l.AddLast(v)
}

func (l *LinkedList) AddFirst(v interface{}) *LinkedListIterator {
  n := new(linkedListNode)
  n.value = v
  if l.first == nil {
    l.last = n
  } else {
    l.first.prev = n
    n.next = l.first
  }
  l.first = n
  l.size ++

  i := NewLinkedListIterator()
  i.list = l
  i.node = n
  return i
}

func (l *LinkedList) AddLast(v interface{}) *LinkedListIterator {
  n := new(linkedListNode)
  n.value = v
  if l.last == nil {
    l.first = n
  } else {
    l.last.next = n
    n.prev = l.last
  }
  l.last = n
  l.size ++

  i := NewLinkedListIterator()
  i.list = l
  i.node = n
  return i
}

func (l *LinkedList) GetFirst() *LinkedListIterator {
  li := NewLinkedListIterator()
  li.list = l
  li.node = l.first
  return li
}

func (l *LinkedList) GetLast() *LinkedListIterator {
  li := NewLinkedListIterator()
  li.list = l
  li.node = l.last
  return li
}

func (l *LinkedList) GetSize() int64 {
  return l.size
}

func (l *LinkedList) IsEmpty() bool {
  return l.size == 0
}

func (l *LinkedList) Join(sep string) string {
  sa := make([]string, l.GetSize())
  n := 0
  for li := l.GetFirst(); !li.IsNull(); li.Step() {
    sa[n] = As(li.GetValue(), Type_string).(string)
    n++
  }
  return JoinStringSlice(sa, sep)
}

func (l *LinkedList) Pop() interface{} {
  if l.first == nil {
    return nil
  } else {
    v := l.first.value
    l.removeNode(l.first)
    return v
  }
}

func (l *LinkedList) PopLast() interface{} {
  if l.last == nil {
    return nil
  } else {
    v := l.last.value
    l.removeNode(l.last)
    return v
  }
}

func (l *LinkedList) ToString() string {
  return ConcatStrings("[", l.Join(","), "]")
}

func (l *LinkedList) removeNode(n *linkedListNode) {
  if n.prev == nil {
    l.first = n.next
  } else {
    n.prev.next = n.next
  }
  if n.next != nil {
    l.last = n.prev
  } else {
    n.next.prev = n.prev
  }
  l.size--
}

func NewLinkedListIterator() *LinkedListIterator {
  return new(LinkedListIterator).Init()
}

func (lli *LinkedListIterator) Init() *LinkedListIterator {
  lli.type_ = LinkedListIteratorType
  return lli
}

func (lli *LinkedListIterator) AddAfter(value interface{}) *LinkedListIterator {
  n := new(linkedListNode)
  if lli.IsLast() {
    lli.list.last = n
  }
  n.value = value
  n.prev = lli.node
  n.next = lli.node.next
  lli.node.next = n
  n.next.prev = n
  lli.list.size++
  i := NewLinkedListIterator()
  i.list = lli.list
  i.node = n
  return i
}

func (lli *LinkedListIterator) AddBefore(value interface{}) *LinkedListIterator {
  n := new(linkedListNode)
  if lli.IsFirst() {
    lli.list.first = n
  }
  n.value = value
  n.prev = lli.node.prev
  n.next = lli.node
  lli.node.prev = n
  n.prev.next = n
  lli.list.size++
  i := NewLinkedListIterator()
  i.list = lli.list
  i.node = n
  return i
}

func (lli *LinkedListIterator) GetLinkedList() *LinkedList {
  return lli.list
}

func (lli *LinkedListIterator) GetNext() *LinkedListIterator {
  i := NewLinkedListIterator()
  i.list = lli.list
  if !lli.IsLast() {
    i.node = lli.node.next
  }
  return i
}

func (lli *LinkedListIterator) GetPrev() *LinkedListIterator {
  i := NewLinkedListIterator()
  i.list = lli.list
  if !lli.IsFirst() {
    i.node = lli.node.prev
  }
  return i
}

func (lli *LinkedListIterator) GetValue() interface{} {
  return lli.node.value
}

func (lli *LinkedListIterator) IsFirst() bool {
  return (lli != nil) && (lli.node != nil) && (lli.list != nil) && (lli.node == lli.list.first)
}

func (lli *LinkedListIterator) IsLast() bool {
  return (lli != nil) && (lli.node != nil) && (lli.list != nil) && (lli.node == lli.list.last)
}

func (lli *LinkedListIterator) IsNull() bool {
  return (lli.list == nil) || (lli.node == nil)
}

func (lli *LinkedListIterator) Remove() *LinkedListIterator {
  lli.list.removeNode (lli.node)
  lli.list = nil
  return lli
}

func (lli *LinkedListIterator) SetLinkedList(l *LinkedList) *LinkedListIterator {
  lli.list = l
  if l.IsEmpty() {
    lli.node = nil
  } else {
    lli.node = l.first
  }
  return lli
}

func (lli *LinkedListIterator) SetValue(value interface{}) *LinkedListIterator {
  lli.node.value = value
  return lli
}

func (lli *LinkedListIterator) Step() *LinkedListIterator {
  if lli.IsNull() {
    return nil
  } else {
    lli.node = lli.node.next
    return lli
  }
}

func (lli *LinkedListIterator) StepBack() *LinkedListIterator {
  if lli.IsNull() {
    return nil
  } else {
    lli.node = lli.node.prev
    return lli
  }
}

func initLinkedList() {
  LinkedListType = RegisterType(reflect.TypeOf((*LinkedList)(nil)).Elem())
  LinkedListIteratorType = RegisterType(reflect.TypeOf((*LinkedListIterator)(nil)).Elem())
}

func initLinkedListCoca() {
}
