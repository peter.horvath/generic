package generic

import (
  "reflect"
)

type Boolean struct {
  Object
  val bool
}

var Type_boolean *Type
var BooleanType *Type

func NewBoolean(b bool) *Boolean {
  return new(Boolean).Init(b)
}

func (b *Boolean) Init(bb bool) *Boolean {
  b.type_ = BooleanType
  b.val = bb
  return b
}

func (t *Boolean) GetValue() bool {
  return t.val
}

func (t *Boolean) SetValue(b bool) {
  t.val = b;
}

func (t *Boolean) Clone() *Boolean {
  return NewBoolean(t.val)
}

func Compare_boolean(a interface{}, b interface{}) int {
  A := a.(bool)
  B := b.(bool)
  if A == B {
    return 0
  } else if A && !B {
    return 1
  } else {
    return -1
  }
}

func (t *Boolean) Compare(b interface{}) int {
  return CompareBoolean(t, b.(*Boolean))
}

func CompareBoolean(a interface{}, b interface{}) int {
  if a.(Boolean).val && !b.(Boolean).val {
    return -1
  } else if !a.(Boolean).val && b.(Boolean).val {
    return 1
  } else {
    return 0
  }
}

func (t *Boolean) ToString() *String {
  return NewString(Convert_boolean_string(t.val).(string))
}

func Convert_boolean_string(a interface{}) interface{} {
  if a.(bool) {
    return "true"
  } else {
    return "false"
  }
}

func Convert_Boolean_string(a interface{}) interface{} {
  return Convert_boolean_string(a.(Boolean).val)
}

func initBoolean() {
  Type_boolean = RegisterType(reflect.TypeOf((*bool)(nil)).Elem())
  BooleanType = RegisterType(reflect.TypeOf((*Boolean)(nil)).Elem())
}

func initBooleanCoca() {
  RegisterComparator(Type_boolean, Type_boolean, Compare_boolean)
  RegisterComparator(BooleanType, BooleanType, CompareBoolean)
  RegisterConverter(Type_boolean, Type_string, Convert_boolean_string)
  RegisterConverter(BooleanType, StringType, Convert_Boolean_string)
}
