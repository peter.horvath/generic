package generic

import (
  "reflect"
)

type Pair struct {
  Object
  key interface{}
  value interface{}
}

var PairType *Type

func NewPair(key interface{}, value interface{}) *Pair {
  return new(Pair).Init(key, value)
}

func (p *Pair) Init(key interface{}, value interface{}) *Pair {
  p.type_ = PairType
  p.key = key
  p.value = value
  return p
}

func (p *Pair) Compare(a interface{}) int {
  return Compare_Pair_Pair(p, a)
}

func (p *Pair) ToString() string {
  pstr := As(p.key, Type_string).(string)
  vstr := As(p.value, Type_string).(string)
  return ConcatStrings(pstr, ":", vstr)
}

func Compare_Pair_Pair_byKey(aa interface{}, bb interface{}) int {
  return Compare(aa.(*Pair).key, bb.(*Pair).key)
}

func Compare_Pair_Pair(aa interface{}, bb interface{}) int {
  r := Compare_Pair_Pair_byKey(aa, bb)
  if r != 0 {
    return r
  } else {
    return Compare(aa.(*Pair).value, bb.(*Pair).value)
  }
}

func Compare_Pair_Key(pair interface{}, key interface{}) int {
  pairKey := pair.(*Pair)
  return Compare(pairKey, key)
}

func Compare_Key_Pair(key interface{}, pair interface{}) int {
  pairKey := pair.(*Pair).key
  return Compare(key, pairKey)
}

func Convert_Pair_string(aa interface{}) interface{} {
  //fmt.Printf("ConvertPairstring: aa type: %s aa kind: %s\n", reflect.TypeOf(aa).Name(), reflect.TypeOf(aa).Kind().String())
  //fmt.Printf("ConvertPairstring: aa elem type: %s aa elem kind: %s\n", reflect.TypeOf(aa).Elem().Name(), reflect.TypeOf(aa).Elem().Kind().String())
  return aa.(*Pair).ToString()
}

func Convert_Pair_String(aa interface{}) interface{} {
  return NewString(Convert_Pair_string(aa).(string))
}

func initPair() {
  PairType = RegisterType(reflect.TypeOf((*Pair)(nil)).Elem())
}

func initPairCoca() {
  RegisterComparator(PairType, PairType, Compare_Pair_Pair)
  RegisterConverter(PairType, Type_string, Convert_Pair_string)
  RegisterConverter(PairType, StringType, Convert_Pair_String)
}
