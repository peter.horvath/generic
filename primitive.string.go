package generic

import (
  "reflect"
)

var Type_string *Type
var StringType *Type

type String struct {
  Object
  val string
}

func NewString(s string) *String {
  return new(String).Init(s)
}

func (s *String) Init(ss string) *String {
  s.type_ = StringType
  s.val = ss
  return s
}

func (t *String) GetValue() string {
  return t.val
}

func (t *String) SetValue(s *String) {
  t.val = s.val;
}

func (t *String) Clone() interface{} {
  return NewString(t.val)
}

func (t *String) Compare(s interface{}) int {
  return CompareString(t, s.(*String))
}

func Compare_strings(a string, b string) int {
  if a < b {
    return -1
  } else if a > b {
    return 1
  } else {
    return 0
  }
}

func Compare_string(a interface{}, b interface{}) int {
  return Compare_strings(a.(string), b.(string))
}

func CompareString(a interface{}, b interface{}) int {
  return Compare_strings(a.(*String).val, b.(*String).val)
}

func Compare_String_string(a interface{}, b interface{}) int {
  return Compare_strings(a.(string), b.(*String).val)
}

func Compare_string_String(a interface{}, b interface{}) int {
  return Compare_strings(a.(*String).val, b.(string))
}

func (t *String) ToString() *String {
  return t.Clone().(*String)
}

func (t *String) Concat(s *String) *String {
  return NewString(ConcatStrings(t.val, s.val))
}

func initString() {
  Type_string = RegisterType(reflect.TypeOf((*string)(nil)).Elem())
  StringType = RegisterType(reflect.TypeOf((*String)(nil)).Elem())
}

func initStringCoca() {
  RegisterComparator(Type_string, Type_string, Compare_string)
  RegisterComparator(StringType, StringType, CompareString)
  RegisterComparator(Type_string, StringType, Compare_String_string)
  RegisterComparator(StringType, Type_string, Compare_string_String)
}
