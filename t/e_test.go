package thetest

import (
  "fmt"

  "testing"
)

type A struct {
  name string
  n int
}

func B(c interface{}) *A {
  return c.(*A)
}

func NewA(name string, n int) *A {
  r := new(A)
  r.name = name
  r.n = n
  return r
}

func (a *A) show() {
  fmt.Println("%s: %d\n", a.name, a.n);
}

func TestE(t *testing.T) {
  d := NewA("A", 5)
  var e interface{}
  e = d
  fmt.Println("%d\n", B(e).n)
  B(e).n++
  fmt.Println("%d\n", d.n)
  f := B(e)
  f.n++
  d.show()
}
