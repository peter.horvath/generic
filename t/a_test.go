package thetest

import (
  "fmt"
  "testing"
  //"reflect"
  "unicode/utf8"
)

func init() {
  fmt.Println("t.init()")
}

func getnil() interface{} {
  return nil
}

//var a reflect.Type = reflect.TypeOf(new(int32))
//var b reflect.Type = reflect.TypeOf(new(int64))


func dumper(n string) {
  fmt.Println(n)
}

func tt(sa ...string) {
  fmt.Printf("Got %d arguments\n", len(sa))
}

func ustrlen(s string) {
  fmt.Printf("Byte length: %d, character length: %d\n", len(s), utf8.RuneCountInString(s))
}

func TestMain(test *testing.T) {
  t := getnil()
  fmt.Printf("cica %p", t)

  tt("a", "b", "c")

  ustrlen("árvíztűrő tükörfúrógép")

  /*
  if a < b {
    fmt.Println("done")
  }*/
}
