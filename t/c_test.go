package thetest

import (
  "reflect"
  "fmt"
  "testing"
)

type testIf interface {
  printThis()
}

type tests struct {
  testIf
  n int
}

func NewTests(n int) *tests {
  r := new(tests)
  r.n = n
  return r
}

func (thisS *tests) printThis() {
  fmt.Printf("%d\n", thisS.n)
}

func TestTypes(t *testing.T) {
  a := 5
  b := "cica"
  fmt.Println("TestTypes")

  s := NewTests(5)
  var i testIf = s

  i.printThis()

  fmt.Println("type of i: " + reflect.TypeOf(i).Elem().Name())

  fmt.Println(reflect.TypeOf(b).Name())
  fmt.Println(reflect.TypeOf(a).Name())
}
