package generic

import (
  "fmt"
  "strconv"

  "testing"
)

func cocaSerializer(i interface{}) string {
  c := i.(*coca)
  res := "{ \"type1\": \"" + c.type1.name + "\",\"type2\": \"" + c.type2.name + "\",\"typeHash\":\"0x" + strconv.FormatInt(int64(c.hash), 16)
  res += "\",\"comparator\":\"" + GetFunctionName(c.comparator) + "\",\"converter\":\"" + GetFunctionName(c.converter) + "\"}"
  return res
}

func TestCoca(t *testing.T) {
  typeLock.Lock()
  str := cocaMap.ToStringFn(cocaSerializer)
  typeLock.Unlock()
  fmt.Println(JsonPrettyPrint(str))
}
